import unittest
import planes_overlord as p_o


class PlanesOverlordTest(unittest.TestCase):

    def test_get_random_coords(self):
        overlord = p_o.PlanesOverlord()
        random_coords = [overlord.get_random_coords() for _ in range(30)]
        for coords in random_coords:
            self.assertTrue(coords[2] > overlord.excluded_arrival_alt[1] and (coords[0] == 0 or coords[1] == 0))

    def test_get_free_arrival_coords(self):
        overlord = p_o.PlanesOverlord()
        for _ in range(30):
            overlord.get_and_register_new_plane()
        for _ in range(1000):
            self.assertFalse(overlord.get_free_arrival_coords() in overlord.planes_entry_coords)

    def test_get_new_plane(self):
        overlord = p_o.PlanesOverlord()
        initial_size = len(overlord.planes_entry_coords)
        for _ in range(30):
            overlord.get_and_register_new_plane()
        self.assertTrue(len(overlord.planes_entry_coords) - initial_size == 30)


if __name__ == '__main__':
    unittest.main()
