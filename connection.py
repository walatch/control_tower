import socket as soc
import json


class Connection:

    def __init__(self, host, port, buffer, encoder, connection_type="SERVER"):
        self.host = host
        self.port = port
        self.buffer = buffer
        self.encoder = encoder
        self.connection_type = connection_type
        self.socket = None
        self.connection = None
        self.address = None

    def __enter__(self):
        # Setting up server socket and connecting 
        if self.connection_type == "SERVER":
            self.socket = soc.socket(soc.AF_INET, soc.SOCK_STREAM)
            self.socket.bind((self.host, self.port))
            self.socket.listen()
            self.connection, self.address = self.socket.accept()
        if self.connection_type == "CLIENT":
            self.connection = soc.socket(soc.AF_INET, soc.SOCK_STREAM)
            self.connection.connect((self.host, self.port))
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.connection.close()
        del self

    def send_json(self, data):
        package = json.dumps(data, indent=2)
        self.connection.send(package.encode(self.encoder))

    def send_raw(self, data):
        self.connection.send(data.encode(self.encoder))

    def recv_json(self):
        return json.loads(self.connection.recv(self.buffer).decode(self.encoder))

    def recv_raw(self):
        return self.connection.recv(self.buffer).decode(self.encoder)
