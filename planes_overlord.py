import random
import plane
from configparser import ConfigParser


class PlanesOverlord:

    @staticmethod
    def get_config():
        cp = ConfigParser()
        cp.read("config.ini")
        return cp

    def __init__(self):
        # CONFIG
        self.config = self.get_config()
        self.airspace_dimensions = {"X": int(self.config["AIRSPACE_CONFIG"]["X"]),
                                    "Y": int(self.config["AIRSPACE_CONFIG"]["Y"]),
                                    "Z": int(self.config["AIRSPACE_CONFIG"]["Z"])}
        self.excluded_arrival_alt = (int(self.config["AIRSPACE_CONFIG"]["EXCLUSION_FLOOR"]),
                                     int(self.config["AIRSPACE_CONFIG"]["EXCLUSION_CEILING"]))
        self.min_dist_separation = int(self.config["AIRSPACE_CONFIG"]["MIN_DIST_SEPARATION"])

        # STORAGE
        self.planes = set()
        self.planes_entry_coords = set()  # TODO flushing first indices after plane leaves entry zone

    def get_and_register_new_plane(self):
        """ """
        new_plane_coords = self.get_free_arrival_coords()
        self.planes_entry_coords.add(new_plane_coords)
        new_plane_obj = plane.Plane(new_plane_coords)
        self.planes.add(new_plane_obj)
        return new_plane_obj

    def get_free_arrival_coords(self):
        """ """
        not_free_coord = True
        x, y, z = self.get_random_coords()
        while not_free_coord:
            x, y, z = self.get_random_coords()
            not_free_coord = self.check_if_coord_is_used(x, y, z)
        return x, y, z

    def check_if_coord_is_used(self, x, y, z):
        """ """
        not_free_coord = False
        for plane in self.planes_entry_coords:
            if (x - self.min_dist_separation * 2 < plane[0] < x + self.min_dist_separation * 2) and \
                    (y - self.min_dist_separation * 2 < plane[1] < y + self.min_dist_separation * 2) and \
                    (z - self.min_dist_separation * 2 < plane[2] < z + self.min_dist_separation * 2):
                print(f"free coord check failed with {x, y, z} and would collide with {plane}")
                not_free_coord = True
                break
        return not_free_coord

    def get_random_coords(self):
        x_or_y = random.choice(("X", "Y"))
        random_coord = random.randrange(0, self.airspace_dimensions[x_or_y] + 1)
        x = 0 if x_or_y == "Y" else random_coord
        y = 0 if x_or_y == "X" else random_coord
        z = random.randint(self.excluded_arrival_alt[1] + 1, self.airspace_dimensions["Z"] + 1)
        return x, y, z


if __name__ == '__main__':
    p_o = PlanesOverlord()
    for i in range(5000):
        p_o.get_and_register_new_plane()
