import datetime as dt
import itertools


class Plane:

    next_id = itertools.count()

    def __init__(self, arrival_coords, fuel=10800, speed=25):
        self.plane_id = next(self.next_id) + 1
        self.arrival_time = int(dt.datetime.now().timestamp())

        self.initial_fuel = fuel
        self.fuel = fuel  # 3h of fuel in seconds // 1 unit of fuel per second of flight
        self.speed = speed

        self.status = "ARRIVAL"

        self.coords = arrival_coords
        self.destination = None
        self.last_tower_contact = None

    # GETTERS
    def update_fuel(self):
        self.fuel = self.initial_fuel - int(dt.datetime.now().timestamp()) - self.arrival_time
        print(self.fuel)

    def get_fuel_info(self):
        self.update_fuel()
        return self.fuel

    def get_coords(self):
        return self.coords

    def get_id_and_coords(self):
        return {'PLANE_ID': self.plane_id, 'COORDS': self.coords}

    def get_status(self):
        return self.status

    def get_destination(self):
        return self.destination

    # SETTERS
    def set_status(self, status):
        self.status = status

    def set_status_landed(self):
        self.status = "LANDED"

    def set_destination(self, coord):
        self.destination = coord
