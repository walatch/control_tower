import unittest
import planes_overlord as p_o
from plane import Plane
import time


class PlaneTest(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_if_planes_have_diff_IDs_and_coords(self):
        overlord = p_o.PlanesOverlord()
        planes = [Plane(overlord.get_free_arrival_coords()) for _ in range(50)]
        for plane in planes:
            print(plane.arrival_time)
        planes_IDs = [plane.plane_id for plane in planes]
        planes_coords = [plane.coords for plane in planes]
        self.assertTrue(len(planes_IDs) == len(set(planes_IDs)) and len(planes_coords) == len(set(planes_coords)))

    def test_if_fuel_is_deacreasing_with_time(self):
        overlord = p_o.PlanesOverlord()
        plane = Plane(overlord.get_free_arrival_coords())
        time.sleep(2)
        self.assertGreater(plane.initial_fuel, plane.get_fuel_info())


if __name__ == '__main__':
    unittest.main()
