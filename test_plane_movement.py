import unittest
import datetime as dt

import plane_movement
import plane_movement as p_m


class PlaneMov(unittest.TestCase):
    def test_get_vector(self):
        last_coord = (100, 200, 50)
        dest1_coord = (50, 100, 20)
        dest2_coord = (150, 300, 80)
        dest3_coord = (100, 200, 50)
        self.assertEqual(p_m.get_vector(last_coord, dest1_coord), (-50, -100, -30))  # add assertion here
        self.assertEqual(p_m.get_vector(last_coord, dest2_coord), (50, 100, 30))
        self.assertEqual(p_m.get_vector(last_coord, dest3_coord), (0, 0, 0))

    def test_get_flight_path(self):
        last_coord = (10, 10, 10)
        dest1_coord = (13, 13, 13)
        path1 = [(11, 11, 11), (12, 12, 12), (13, 13, 13)]
        self.assertEqual(p_m.get_flight_path(last_coord, dest1_coord), path1)
        dest2_coord = (7, 7, 7)
        path2 = [(9, 9, 9), (8, 8, 8), (7, 7, 7)]
        self.assertEqual(p_m.get_flight_path(last_coord, dest2_coord), path2)
        dest3_coord = (10, 13, 7)
        path3 = [(10, 11, 9), (10, 12, 8), (10, 13, 7)]
        self.assertEqual(p_m.get_flight_path(last_coord, dest3_coord), path3)
        dest4_coord = (10, 12, 6)
        path4 = [(10, 11, 9), (10, 12, 8), (10, 12, 7), (10, 12, 6)]
        self.assertEqual(p_m.get_flight_path(last_coord, dest4_coord), path4)

    def test_get_route_history(self):
        st_coord = (10, 10, 10)
        end_coord = (15, 15, 15)
        st_time = int(dt.datetime.now().timestamp())
        end_time = st_time + 10  #
        p_speed = 2
        exp1_path_hist = [((11, 11, 11), st_time), ((12, 12, 12), st_time), ((13, 13, 13), st_time + 1),
                          ((14, 14, 14), st_time +1), ((15, 15, 15), st_time + 2)]
        self.assertEqual(p_m.get_route_history(st_coord, end_coord, st_time, end_time, p_speed), exp1_path_hist)


if __name__ == '__main__':
    unittest.main()
