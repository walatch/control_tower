

def get_vector(last_coord, dest_coord):
    """ """
    x = dest_coord[0] - last_coord[0]
    y = dest_coord[1] - last_coord[1]
    z = dest_coord[2] - last_coord[2]
    return x, y, z


def get_flight_path(last_coord, dest_coord):
    vect = get_vector(last_coord, dest_coord)
    x = [last_coord[0]] if vect[0] == 0 else \
        list(range(last_coord[0] + 1, last_coord[0] + vect[0] + 1)) if vect[0] > 0 else \
            list(reversed(range(last_coord[0] + vect[0], last_coord[0])))
    y = [last_coord[1]] if vect[1] == 0 else \
        list(range(last_coord[1] + 1, last_coord[1] + vect[1] + 1)) if vect[1] > 0 else \
            list(reversed(range(last_coord[1] + vect[1], last_coord[1])))
    z = [last_coord[2]] if vect[2] == 0 else \
        list(range(last_coord[2] + 1, last_coord[2] + vect[2] + 1)) if vect[2] > 0 else \
            list(reversed(range(last_coord[2] + vect[2], last_coord[2])))

    max_len = max((len(x), len(y), len(z)))
    if len(x) < max_len:
        x.extend([x[-1] for _ in range(max_len - len(x))])
    if len(y) < max_len:
        y.extend([y[-1] for _ in range(max_len - len(y))])
    if len(z) < max_len:
        z.extend([z[-1] for _ in range(max_len - len(z))])

    return list(zip(x, y, z))


def get_route_history(st_coord, end_coord, st_time, end_time, p_speed, step=1):
    path = get_flight_path(st_coord, end_coord)
    # timestamps per plane speed
    t_stamps = [t_stamp for t_stamp in range(st_time, end_time, step) for _ in range(p_speed)]
    return list(zip(path, t_stamps))


