import psycopg2
import sqlite3
from configparser import ConfigParser


def get_config_info_postgresql():
    cp = ConfigParser()
    cp.read("config.ini")
    return (
        cp["POSTREGSQL"]["USER"],
        cp["POSTREGSQL"]["PASSWORD"],
        cp["POSTREGSQL"]["HOST"],
        cp["POSTREGSQL"]["PORT"],
        cp["POSTREGSQL"]["DATABASE"]
    )


def get_config_info_sqlite3():
    cp = ConfigParser()
    cp.read("config.ini")
    return cp["SQLITE3"]["PATH"]


class DB:
    def __init__(self, db_type):
        self.db_type = db_type
        self.q_type = {"postgresql": "%s", "sqlite3": "?"}
        if db_type == "postgresql":
            db_conf = get_config_info_postgresql()
            self.db_conn = psycopg2.connect(
                user=db_conf[0],
                password=db_conf[1],
                host=db_conf[2],
                port=db_conf[3],
                database=db_conf[4])
        if db_type == "sqlite3":
            self.db_conn = sqlite3.connect(r"control_tower.db")

    def register_plane(self, plane):
        cursor = self.db_conn.cursor()        
        query = f"""INSERT INTO planes (plane_id, coords, status) 
        VALUES ({self.q_type[self.db_type]}, {self.q_type[self.db_type]}, {self.q_type[self.db_type]})"""
        cursor.execute(query, (plane.plane_id, plane.coords, plane.status))
        self.db_conn.commit()
        
    def get_plane_info(self, plane_id):
        cursor = self.db_conn.cursor()
        query = f"SELECT plane_id, coords, status FROM planes WHERE plane_id = {self.q_type[self.db_type]}"
        cursor.execute(query, (plane_id,))
        self.db_conn.commit()
        return cursor.fetchone()

    def update_plane_status(self, plane_id, new_status):
        cursor = self.db_conn.cursor()
        query = f"""UPDATE planes SET status = {self.q_type[self.db_type]} 
        WHERE plane_id = {self.q_type[self.db_type]}"""
        cursor.execute(query, (new_status, plane_id))
        self.db_conn.commit()

    
